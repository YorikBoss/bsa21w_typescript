export function createElement({tagName, className, attributes = {}}: {tagName: string, className: string, attributes: {}}): Element {
  const element: Element = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  <void>(Object.keys(attributes)).forEach((key: string): void => element.setAttribute(key, attributes[key]));

  return element;

}

