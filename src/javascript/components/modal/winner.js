import {showModal} from './modal';
import { createElement } from '../../helpers/domHelper';
// import App from '../../app';

export function showWinnerModal(fighter) {
  const winnerModal = createElement({ tagName: 'div', className: 'winner-modal' });
  winnerModal.innerText = `Health: ${fighter.finalHealth}`;
  showModal({title: `winner: ${fighter.name}`, bodyElement: winnerModal, onClose: newApp});
}

function newApp(){
  const arena = document.querySelector('.arena___root');
  arena?.remove();
  // new App();
  window.location.reload();
}
