import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';

export function createFighters(fighters: {}[]): {}[] {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root', attributes: {} });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root', attributes: {}  });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list', attributes: {}  });
  const fighterElements = fighters.map((fighter: {_id: string, name: string, source: string}) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: {_id: string, name: string, source: string}, selectFighter: (x: string, y:string)=> {_id: string, name: string, source: string}):Element {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter', attributes: {} });
  const imageElement = createImage(fighter);
  const onClick = (event: string): {} => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: {_id: string, name: string, source: string}) {
  const { source, name } = fighter;
  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });

  return imgElement;
}