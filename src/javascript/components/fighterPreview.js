import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const avatarElement = createFighterImage(fighter);

    const nameElement = createPropertyOfFighter("NAME", fighter.name);
    const healthElement = createPropertyOfFighter("HEALTH", fighter.health);
    const attackElement = createPropertyOfFighter("ATTACK", fighter.attack);
    const blockElement = createPropertyOfFighter("DEFENSE", fighter.defense);

    fighterElement.append(avatarElement, nameElement, healthElement, attackElement, blockElement);
  }
  return fighterElement;
}

function createPropertyOfFighter(propertyName, propertyData){
  const element = createElement({tagName: 'div'});
  element.innerHTML = `${propertyName}: ${propertyData}`;
  return element;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
