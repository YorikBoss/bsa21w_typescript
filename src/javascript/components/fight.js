import { controls } from '../../constants/controls';
import {time} from '../../constants/time';
import {fighterService} from '../services/fighterService';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    fighterService.addFighterHealth(secondFighter);
    fighterService.addFighterBlock(firstFighter);
    fighterService.addFighterHealth(firstFighter);
    fighterService.addFighterBlock(secondFighter);
    fighterService.addFighterSuperHit(firstFighter);
    fighterService.addFighterSuperHit(secondFighter);
    fighterService.addFighterSuperHitPause(firstFighter);
    fighterService.addFighterSuperHitPause(secondFighter);

    document.addEventListener('keydown', forKeyDown);
    document.addEventListener('keyup', forKeyUp);

    function forKeyDown(event){
      onKeyDown(event, firstFighter, secondFighter, resolve)
    }
    
    function forKeyUp(event){
      onKeyUp(event, firstFighter, secondFighter)
    }
  });

 
}



export function getDamage(attacker, defender) {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return  fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const randomNumber  = Math.random() + 1;
  return fighter.isBlock? fighter.defense * randomNumber : 0;
}

function onKeyDown(event, firstFighter, secondFighter, resolve) {
  const [firstKeyPlayerOne, secondKeyPlayerOne, thirdKeyPlayerOne] = controls.PlayerOneCriticalHitCombination;
  const [firstKeyPlayerTwo, secondKeyPlayerTwo, thirdKeyPlayerTwo] = controls.PlayerTwoCriticalHitCombination;
  const PlayerOneKeyCombination = [controls.PlayerOneAttack, controls.PlayerOneBlock, ...controls.PlayerOneCriticalHitCombination];
  const PlayerTwoKeyCombination = [controls.PlayerTwoAttack, controls.PlayerTwoBlock, ...controls.PlayerTwoCriticalHitCombination];

  const firstPlayerIndicatorQuerySelector = "#left-fighter-indicator";
  const secondPlayerIndicatorQuerySelector = "#right-fighter-indicator";

  let isPlayerOneKeyDown = PlayerOneKeyCombination.some((element)=>{
    return event.code == element;
  });

  if (isPlayerOneKeyDown) {    
    playerKeyDownAction(firstFighter, secondFighter, event.code, 
      {
        "attack": controls.PlayerOneAttack,
        "block": controls.PlayerOneBlock,
        "firstKey": firstKeyPlayerOne, 
        "secondKey": secondKeyPlayerOne,
        "thirdKey": thirdKeyPlayerOne
      }, secondPlayerIndicatorQuerySelector, controls.PlayerOneCriticalHitCombination, resolve);
  }
  
  let isPlayerTwoKeyDown = PlayerTwoKeyCombination.some((element)=>{
    return event.code == element;
  });

  if (isPlayerTwoKeyDown) {    
    playerKeyDownAction(secondFighter, firstFighter, event.code, 
      {
        "attack": controls.PlayerTwoAttack,
        "block": controls.PlayerTwoBlock,
        "firstKey": firstKeyPlayerTwo, 
        "secondKey": secondKeyPlayerTwo,
        "thirdKey": thirdKeyPlayerTwo
      }, firstPlayerIndicatorQuerySelector, controls.PlayerTwoCriticalHitCombination, resolve);
  }
}

function onKeyUp(event, firstFighter, secondFighter) {

  const [firstKeyPlayerOne, secondKeyPlayerOne, thirdKeyPlayerOne] = controls.PlayerOneCriticalHitCombination;
  const [firstKeyPlayerTwo, secondKeyPlayerTwo, thirdKeyPlayerTwo] = controls.PlayerTwoCriticalHitCombination;

  const PlayerOneKeyCombination = [controls.PlayerOneBlock, ...controls.PlayerOneCriticalHitCombination];
  const PlayerTwoKeyCombination = [controls.PlayerTwoBlock, ...controls.PlayerTwoCriticalHitCombination];
  
  let isPlayerOneKeyUp = PlayerOneKeyCombination.some((element)=>{
    return event.code == element;
  });

  if (isPlayerOneKeyUp) {    
    playerKeyUpAction(firstFighter, event.code,
      {
        "block": controls.PlayerOneBlock,
        "firstKey": firstKeyPlayerOne, 
        "secondKey": secondKeyPlayerOne,
        "thirdKey": thirdKeyPlayerOne
      })
  }

  let isPlayerTwoKeyUp = PlayerTwoKeyCombination.some((element)=>{
    return event.code == element;
  });

  if (isPlayerTwoKeyUp) {    
    playerKeyUpAction(secondFighter, event.code,
      {
        "block": controls.PlayerTwoBlock,
        "firstKey": firstKeyPlayerTwo, 
        "secondKey": secondKeyPlayerTwo,
        "thirdKey": thirdKeyPlayerTwo
      })
  }
}

function playerKeyUpAction(fighter, code, keysCombination ){
  if (code == keysCombination.block) {
    fighter.setBlock(false);
  }
  if(code == keysCombination.firstKey || code == keysCombination.secondKey || code == keysCombination.thirdKey){
    fighter.deleteCombination(code);
  }
}

function playerKeyDownAction(fighter, enemy, code, keysCombination, enemyIndicatorQuerySelector, criticalHitCombination, resolve){
  let isAttack = (code == keysCombination.attack);
  if (isAttack) {
    if (!fighter.isBlock) {
      setDamage(fighter, enemy);
      setFighterIndicator(enemy, enemyIndicatorQuerySelector);
      setResolve(fighter, enemy, resolve);
    }
  }

  let isBlock = (code == keysCombination.block);
  if (isBlock) {
    fighter.setBlock(true);
  }

  let isFirstKey = (code == keysCombination.firstKey);
  let isSecondKey = (code == keysCombination.secondKey);
  let isThirdKey = (code == keysCombination.thirdKey);
  if(isFirstKey || isSecondKey || isThirdKey){
    if(!fighter.isSuperHitPause){
      setCombination(fighter, enemy, code, criticalHitCombination);
      setFighterIndicator(enemy, enemyIndicatorQuerySelector);
      setResolve(fighter, enemy, resolve);
    }
  }
}

function setFighterIndicator(fighter, playerIndicatorQuerySelector){
  const playerIndicator = document.querySelector(playerIndicatorQuerySelector);
  playerIndicator.style.width=`${fighter.finalHealth/fighter.health*100}%`;
}

function setResolve(attacker, defender, resolve){
  if(defender.finalHealth === 0){
    return resolve(attacker);
  }
}

function setDamage(attacker, defender){
  const damage = getDamage(attacker, defender);
  defender.setFinalHealth(damage);
}

function setSuperDamage(attacker, defender){
  defender.setFinalHealth(attacker.attack * 2);
  attacker.setSuperHitPause(true);
  setTimeout(()=>{
    attacker.setSuperHitPause(false);
  }, time.CriticalHitMillisecondsInterval);

};


function setCombination(attacker, defender, eventCode, codes){
  attacker.addCombination(eventCode);
  
  let isCriticalCombination = codes.every(element => {
    return attacker.combination.has(element);
  });

  if(!isCriticalCombination){
    return;
  }
  
  attacker.clearCombination();

  setSuperDamage(attacker, defender);
}



